import math

from genetic.algorithm import *
from genetic.specimen import *


def int_to_genes(val):
    return list(map(lambda a: int(a), ('{0:08b}'.format(val))))


def genes_to_int(genes):
    i = len(genes) - 1
    res = 0
    for bit in genes:
        res += bit.value * 2 ** i
        i -= 1
    return res


def f(x):
    return x ** 2 / (math.sin(x) + 2)


target = 10


def adaptation(x):
    return -math.fabs(f(genes_to_int(x.genes)) - target)


def mutate(val):
    return 1 - val


if __name__ == "__main__":
    base_population = list()
    for num in range(0, 20, 2):
        genes = list(map(lambda a: Gene(a, mutate), int_to_genes(num)))
        base_population.append(Specimen(genes))

    res = genetic(adaptation, base_population, 10, 0)

    x = genes_to_int(res[-1].genes)
    print("Result x: ", x)
    print("f(x) = ", f(x))
    print("Deviation:", math.fabs(target - f(x)))
