import copy
import random


class Gene:
    def __init__(self, value, mutate_func):
        self.value = value
        self.mutate_func = mutate_func

    def mutate(self):
        self.value = self.mutate_func(self.value)


class Specimen:
    def __init__(self, genes):
        self.genes = genes

    def reproduce(self, other, amount=0, chance=0.2):
        if amount == 0:
            amount = random.randint(1, len(self.genes))

        dots = list()
        positions = range(len(self.genes))
        for i in range(amount):
            dots.append(positions[random.randint(1, len(positions) - 1)])
        dots.sort()

        prev = 0
        genes1 = list()
        genes2 = list()
        for i in range(len(dots)):
            if i % 2 == 0:
                genes1 += copy.deepcopy(self.genes[prev:dots[i]])
                genes2 += copy.deepcopy(other.genes[prev:dots[i]])
            else:
                genes1 += copy.deepcopy(other.genes[prev:dots[i]])
                genes2 += copy.deepcopy(self.genes[prev:dots[i]])
            prev = dots[i]
        if prev % 2 == 0:
            genes1 += copy.deepcopy(other.genes[prev:])
            genes2 += copy.deepcopy(self.genes[prev:])
        else:
            genes1 += copy.deepcopy(self.genes[prev:])
            genes2 += copy.deepcopy(other.genes[prev:])

        for i in range(len(genes1)):
            if random.random() < chance:
                genes1[i].mutate()
            if random.random() < chance:
                genes2[i].mutate()

        return Specimen(genes1), Specimen(genes2)
