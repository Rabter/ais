import random


def genetic(adapt_func, population, tmax, adapt_value=None):
    t = 0
    while t < tmax and adapt_value != adapt_func(population[-1]):
        old_population = len(population)
        for i in range(old_population):
            pair = random.randint(0, old_population - 1)
            if i != pair:
                population.extend(population[i].reproduce(population[pair]))
        population.sort(key=adapt_func)
        population = population[-10:]
        t += 1
    return population
