import math

from recommendation_system.tree.tree import *
from recommendation_system.tree.from_json import load_tree


def manhattan_metric(table, id1, id2):
    dist = 0
    for i in range(2, len(table[id1])):
        dist += abs(table[id1][i] - table[id2][i])
    return dist


def euclidean_metric(table, id1, id2):
    dist = 0
    for i in range(2, len(table[id1])):
        dist += pow(table[id1][i] - table[id2][i], 2)
    return math.sqrt(dist)


def tree_metric(table, id1, id2):
    tree = load_tree("dataset/tree.json")
    subtrees = tree.children()
    first = Tree(None)
    second = Tree(None)
    for el in subtrees:
        if el.node == table[id1][1]:
            first = el
        if el.node == table[id2][1]:
            second = el
    return tree_distance(first, second)


def L(vec):
    return len(list(filter(lambda num: num != 0, vec)))


def U(vec1, vec2):
    count = 0
    for i in range(len(vec1)):
        if vec1[i] != 0 and vec2[i] != 0:
            count += 1
    return count


def R(vec1, vec2):
    count = 0
    for i in range(len(vec1)):
        if vec1[i] == 0 and vec2[i] == 0:
            count += 1
    return count


def correlation_metric(table, id1, id2):
    A = table[id1][2:]
    B = table[id2][2:]
    a = L(A)
    b = L(B)
    c = U(A, B)
    d = R(A, B)
    return (c + d) / (a + b + d - c)
