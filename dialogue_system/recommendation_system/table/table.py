class Table(list):
    def __init__(self, spaces=5):
        super().__init__()
        self.format = list()
        self.headers = list()
        self.spaces = spaces

    def load(self, filename):
        file = open(filename, 'r', encoding="utf-8")
        self.headers = ["№"] + file.readline().strip('\n').split(';')
        n = 0
        for line in file:
            self.append([str(n)] + line.strip('\n').split(';'))
            n += 1
        self.reformat()

    def reformat(self):
        self.format = list()
        for i in range(len(self.headers)):
            self.format.append(len(self.headers[i]) + self.spaces)
        for line in self:
            for i in range(len(line)):
                if len(line[i]) + self.spaces > self.format[i]:
                    self.format[i] = len(line[i]) + self.spaces

    def print(self):
        self.reformat()
        for i in range(len(self.headers)):
            print(("{:%d}" % self.format[i]).format(self.headers[i]), end='')
        print()
        for line in self:
            for i in range(len(line)):
                print(("{:%d}" % self.format[i]).format(line[i]), end='')
            print()
