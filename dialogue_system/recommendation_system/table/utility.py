from recommendation_system.table.table import Table


def split_tags(table):
    tags = set()
    for line in table:
        current_tags = line[-1].replace(" ", "").split(',')
        for tag in current_tags:
            tags.add(tag)

    res = Table()
    res.headers = table.headers[:-1]
    for tag in tags:
        res.headers.append(tag)

    for line in table:
        data = line[:-1]
        for tag in tags:
            if tag in line[-1].replace(" ", "").split(','):
                data.append('1')
            else:
                data.append('0')
        res.append(data)

    res.reformat()
    return res


def get_attribute_list(table):
    res = list()

    for line in table:
        attr = line[:2]
        if line[2] == "Завершен":
            attr.append(1)
        else:
            attr.append(0)
        for i in range(3, len(line)):
            attr.append(int(line[i]))
        res.append(attr)

    min_year = res[0][3]
    for line in res:
        min_year = min(min_year, line[3])
    for line in res:
        line[3] -= min_year

    max_values = res[0][:]
    for i in range(1, len(res)):
        for j in range(len(res[i])):
            if res[i][j] > max_values[j]:
                max_values[j] = res[i][j]

    for line in res:
        for j in range(2, len(line)):
            if max_values[j] != 0:
                line[j] /= max_values[j]

    return res
