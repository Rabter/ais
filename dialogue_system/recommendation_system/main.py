from recommendation_system.table.utility import *
from recommendation_system.metric import *


class Filters:
    def __init__(self):
        self.status = {"Завершен", "Продолжается"}
        self.years_filter = False
        self.years_range = list()
        self.rating = 0
        self.containing_tags = set()
        self.excluding_tags = set()


def format_result(table, data):
    sim = Table()
    sim.extend(map(lambda a: [str(a)], data))

    for i in range(len(sim)):
        sim[i].insert(0, str(i))
        for j in range(1, len(table[i])):
            sim[i].append(table[i][j])
    sim.sort(key=lambda a: a[1])

    sim.headers = table.headers[:]
    sim.headers.insert(1, "Расстояние")
    sim.reformat()
    return sim


def difference(id, table, metric):
    res = list()
    for i in range(len(table)):
        res.append(metric(table, id, i))
    return res


def recommend(table, likes, dislikes, metric):
    like_res = [0] * len(table)
    unlike_res = [0] * len(table)
    len1 = len2 = 1

    if len(likes):
        for id in likes:
            like_res = [a + b for a, b in zip(like_res, difference(id, table, metric))]
        len1 = len(likes)
    if len(dislikes):
        for id in dislikes:
            unlike_res = [a + b for a, b in zip(unlike_res, difference(id, table, metric))]
        len2 = len(dislikes)

    avg_like = [el / len1 for el in like_res]
    avg_unlike = [el / len2 for el in unlike_res]
    m = max(avg_unlike)
    return [a + m - b for a, b in zip(avg_like, avg_unlike)]


def set_likes():
    likes = list(map(int, input("\nВведите список номеров понравившихся комиксов:\n").replace(" ", "").split(',')))
    dislikes = list(map(int, input("\nВведите список номеров непонравившихся комиксов:\n").replace(" ", "").split(',')))
    return likes, dislikes


def set_metric():
    choice = 0
    wrong = True
    while wrong:
        print("\nВыберите метрику:\n")
        print("1. Евклидовая\n")
        print("2. Манхэттенская\n")
        print("3. Корреляционная\n")
        print("4. По дереву\n")
        choice = int(input()) - 1
        if choice in range(0, 4):
            wrong = False

    return [euclidean_metric, manhattan_metric, correlation_metric, tree_metric][choice]


def set_status():
    print("\nВыберите требуемый статус:")
    print("1. Продолжается")
    print("2. Завершен")
    print("3. Любой")
    choice = input()

    res = set()
    if choice in ('1', '3'):
        res.add("Продолжается")
    if choice in ('2', '3'):
        res.add("Завершен")

    return res


def set_years():
    print("\nВыберите действие:")
    print("1. Задать диапазон")
    print("2. Сбросить фильтр")

    choice = input()
    if choice == '1':
        start = int(input("\nВведите начало диапазона:\n"))
        end = int(input("\nВведите конец диапазона:\n"))
        return True, [start, end]
    return False, list()


def set_rating():
    rating = int(input("\nВведите минимальную оценку (1..5):\n"))
    while not 1 <= rating <= 5:
        rating = int(input("\nОценка долджна быть от 1 до 5, повторите ввод:\n"))
    return rating


def set_tags():
    print("\nВыберите действие:")
    print("1. Задать тэги")
    print("2. Сбросить фильтр")

    choice = input()
    if choice == '1':
        return set(input("\nВведите тэги через запятую:\n").replace(" ", "").lower().split(','))
    else:
        return set()


def set_filters(filters):
    choice = ''
    while choice != '0':
        print("\nВыберите действие:")
        print("0. Назад")
        print("1. Задать фильтр: статус")
        print("2. Задать фильтр: диапазон года выпуска")
        print("3. Задать фильтр: минимальная оценка")
        print("4. Задать фильтр: требуемые тэги")
        print("5. Задать фильтр: исключенные тэги")

        choice = input()
        if choice == '1':
            filters.status = set_status()
        elif choice == '2':
            filters.years_filter, filters.years_range = set_years()
        elif choice == '3':
            filters.rating = set_rating()
        elif choice == '4':
            filters.containing_tags = set_tags()
        elif choice == '5':
            filters.excluding_tags = set_tags()


def filter(recommendations, filters, dataset):
    res = Table()
    res.headers = recommendations.headers[:]

    for el in recommendations:
        keep = True
        id = int(el[0])
        if dataset[id][2] not in filters.status:
            keep = False
        if filters.years_filter and int(dataset[id][3]) not in range(filters.years_range[0], filters.years_range[1]):
            keep = False
        if int(dataset[id][4]) < filters.rating:
            keep = False
        argset = set(dataset[id][5].replace(" ", "").lower().split(','))
        if not argset.issuperset(filters.containing_tags):
            keep = False
        if len(argset.intersection(filters.excluding_tags)) > 0:
            keep = False

        if keep:
            res.append(el)

    res.reformat()
    return res


def search(dataset, likes, dislikes, metric, filters):
    table = split_tags(dataset)
    table = get_attribute_list(table)
    recommendations = format_result(dataset, recommend(table, likes, dislikes, metric))
    filtered = filter(recommendations, filters, dataset)
    extended = False
    if len(filtered) == 0:
        extended = True
        while len(filtered) == 0:
            hardest = len(filters.status) / 2
            ihardest = 0
            min_year = int(min(dataset, key=lambda a: int(a[3]))[3])
            max_year = int(max(dataset, key=lambda a: int(a[3]))[3])
            tmp = (filters.years_range[1] - filters.years_range[0]) / (max_year - min_year)
            if tmp < hardest:
                hardest = tmp
                ihardest = 1
            tmp = 1 - filters.rating / 5
            if tmp < hardest:
                hardest = tmp
                ihardest = 2
            tags_amount = len(table[0]) - 4
            tmp = 1 - len(filters.containing_tags) / tags_amount
            if tmp < hardest:
                hardest = tmp
                ihardest = 3
            tmp = 1 - len(filters.excluding_tags) / tags_amount
            if tmp < hardest:
                ihardest = 4

            if ihardest == 0:
                filters.status = {"Завершен", "Продолжается"}
            elif ihardest == 1:
                filters.years_range[0] -= 1
                filters.years_range[1] += 1
            elif ihardest == 2:
                filters.rating -= 1
            elif ihardest == 3:
                filters.containing_tags.pop()
            elif ihardest == 4:
                filters.excluding_tags.pop()

            filtered = filter(recommendations, filters, dataset)

    return extended, filtered


if __name__ == "__main__":
    table = Table()
    table.load("dataset/table.txt")
    print("Загруженный датасет:")
    table.print()

    likes = []
    dislikes = []
    metric = euclidean_metric
    filters = Filters()

    choice = ''
    while choice != '0':
        print("\nВыберите действие:")
        print("1. Задать понравившиеся комиксы")
        print("2. Задать метрику")
        print("3. Задать фильтры")
        print("4. Поиск")
        choice = input()

        if choice == '1':
            likes, dislikes = set_likes()
        elif choice == '2':
            metric = set_metric()
        elif choice == '3':
            set_filters(filters)
        elif choice == '4':
            extended, res = search(table, likes, dislikes, metric, filters)
            if extended:
                print("\nНе найдено результатов по заданным фильтрам. Возможно, вам подойдут следующие предложения:")
            res.print()
