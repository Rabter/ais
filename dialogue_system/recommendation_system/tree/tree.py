class Tree:
    def __init__(self, node, parent=None, length=0, subtrees=None):
        self.node = node
        self.parent = parent

        self.length = length
        if parent is None:
            self.level = 0
        else:
            self.level = parent.level + 1
            parent.subtrees.append(self)

        if subtrees is None:
            self.subtrees = list()
        else:
            self.subtrees = subtrees

    def new_child(self, child):
        child.parent = self
        self.subtrees.append(child)

    def children(self):
        res = self.subtrees[:]
        for node in self.subtrees:
            res.extend(node.children())
        return res


def tree_distance(first, second):
    res = 0
    while first.level > second.level:
        res += first.length
        first = first.parent

    while second.level > first.level:
        res += second.length
        second = second.parent

    while first != second:
        res += first.length + second.length
        first = first.parent
        second = second.parent

    return res
