import json
from recommendation_system.tree.tree import Tree


def as_tree(dct):
    return Tree(dct['node'], None, dct['distance'], dct['subtrees'])


def fix_tree(node):
    for child in node.subtrees:
        child.level = node.level + 1
        child.parent = node
        fix_tree(child)


def load_tree(filename):
    file = open(filename, 'r', encoding="utf-8")
    s = file.read()
    res = json.loads(s, object_hook=as_tree)
    fix_tree(res)
    return res
