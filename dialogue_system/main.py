import random

import pymorphy2
import re

from itertools import chain

from recommendation_system import main as rs
from recommendation_system.tree import from_json
from damerau import damerau
import responses


def phrase_to_list(phrase):
    return re.sub(r'[^\w\s]', '', phrase).split()


def normalize(splitted):
    normal = list()
    parsed = list()
    analyzer = pymorphy2.MorphAnalyzer()
    for word in splitted:
        normal.append(analyzer.normal_forms(word))
        parsed.append(analyzer.parse(word))
    return normal, parsed


def tokenize(phrase, context):
    words = phrase_to_list(phrase)
    forms, parsed = normalize(words)
    tokens = set()
    all_forms = set(chain.from_iterable(forms))
    default_forms = [el[0] for el in forms]
    args = dict()

    if all_forms.intersection({"рандомный", "случайный", "любой"}):
        tokens.add("random")
    elif all_forms.intersection({"рекомендовать", "порекомендовать", "посоветовать"}):
        tokens.add("recommend")

    if all_forms.intersection({"коммерческий"}):
        tokens.add("commercial")

    if all_forms.intersection({"роман", "книга", "книжка"}):
        tokens.add("novel")

    if all_forms.intersection({"восточный", "азиатский", "азия", "восток"}):
        tokens.add("asian")

    if all_forms.intersection({"манга", "японский", "япония"}):
        tokens.add("manga")

    if all_forms.intersection({"манхва", "корейский", "корея"}):
        tokens.add("manhwa")

    if all_forms.intersection({"маньхуа", "маньхуя", "манхуа", "манхуя", "китайский", "китай"}):
        tokens.add("manhwa")

    if all_forms.intersection({"традиционный", "супергерой", "обычный", "супергеройский", "супергеройка"}):
        tokens.add("traditional")

    if all_forms.intersection({"marvel", "марвел", "марвеловский"}):
        tokens.add("marvel")

    if all_forms.intersection({"dc", "дс", "диси", "дисишный"}):
        tokens.add("dc")

    if "detective" in default_forms and "comics" in default_forms and default_forms.index(
            "detective") + 1 == default_forms.index("comics"):
        tokens.add("dc")

    if all_forms.intersection({"любительский", "некоммерческий", "одиночка", "энтузиаст"}):
        tokens.add("amateur")

    if all_forms.intersection({"веб", "web", "интернет"}):
        tokens.add("web")

    if all_forms.intersection({"стрип", "мем", "картинка"}):
        tokens.add("strip")

    intersection = all_forms.intersection({"про", "жанр", "тег", "тэг"})
    if intersection:
        tokens.add("tag")
        pos = default_forms.index(list(intersection)[0])
        try:
            args["tag"].append(default_forms[pos + 1])
        except KeyError:
            args["tag"] = [default_forms[pos + 1]]

    if all_forms.intersection({"ребёнок", "детский"}):
        tokens.add("tag")
        try:
            args["tag"].append("Детям")
        except KeyError:
            args["tag"] = ["Детям"]

    if context is not None and context == "reask":
        if all_forms.intersection({"нет"}):
            tokens.add("stop")
        if all_forms.intersection("не"):
            pos = default_forms.index(list(intersection)[0]) + 1
            if pos == len(default_forms) or default_forms[pos] in {"надо", "стоит"}:
                tokens.add("stop")

    intersection = all_forms.intersection({"закончить", "окончить", "завершить", "закрыть"})
    if intersection:
        for i in range(len(parsed)):
            if parsed[i][0].normal_form == list(intersection)[0]:
                if "VERB" in parsed[i][0].tag or "INFN" in parsed[i][0].tag:
                    tokens.add("stop")
                else:
                    tokens.add("status")
                    if i > 0 and default_forms[i - 1] in {"не", "кроме"}:
                        args["status"] = "Продолжается"
                    else:
                        args["status"] = "Завершен"

    intersection = all_forms.intersection({"продолжаться", "продолжать", "онгоинг"})
    if intersection:
        for i in range(len(parsed)):
            for parsing in parsed[i]:
                if parsing.normal_form == list(intersection)[0]:
                    tokens.add("status")
                    if i > 0 and default_forms[i - 1] in {"не", "кроме"}:
                        args["status"] = "Завершен"
                    else:
                        args["status"] = "Продолжается"

    intersection = all_forms.intersection({"рейтинг", "оценка", "балл"})
    if intersection:
        pos = default_forms.index(list(intersection)[0])
        found = False
        while pos < len(default_forms) and not found:
            if default_forms[pos].isdigit():
                tokens.add("rating")
                args["rating"] = int(default_forms[pos])
                found = True
            pos += 1

    if all_forms.intersection({"с"}):
        prev = 0
        for i in range(default_forms.count("с")):
            pos = default_forms.index("с", prev)
            prev = pos
            if (pos == 0 or default_forms[pos - 1] != "начинать") and pos < len(default_forms) - 1 and default_forms[
                pos + 1].isdigit():
                tokens.add("minyear")
                args["minyear"] = int(default_forms[pos + 1])

    intersection = all_forms.intersection({"по", "до"})
    if intersection:
        pos = default_forms.index(list(intersection)[0])
        if pos < len(default_forms) - 1 and default_forms[pos + 1].isdigit():
            tokens.add("maxyear")
            args["maxyear"] = int(default_forms[pos + 1])

    if all_forms.intersection({"в"}):
        pos = default_forms.index("в")
        if pos < len(default_forms) - 1 and default_forms[pos + 1].isdigit():
            tokens.add("minyear")
            tokens.add("maxyear")
            args["minyear"] = int(default_forms[pos + 1])
            args["maxyear"] = int(default_forms[pos + 1])

    intersection = all_forms.intersection({"название", "называться"})
    if intersection:
        pos = default_forms.index(list(intersection)[0])
        tokens.add("name")
        args["name"] = phrase[phrase.find(parsed[pos][0].word):]

    if all_forms.intersection({"спасибо", "благодарить"}):
        tokens.add("thanks")

    if all_forms.intersection({"всё", "весь"}):
        if "комикс" in all_forms or len(all_forms.intersection({"что", "быть"})) == 2:
            tokens.add("all")

    if all_forms.intersection({"топ"}):
        pos = default_forms.index("топ")
        if pos < len(default_forms) - 1 and default_forms[pos + 1].isdigit():
            tokens.add("top")
            args["top"] = int(default_forms[pos + 1])

    intersection = all_forms.intersection({"топовый", "лучший"})
    if intersection:
        pos = default_forms.index(list(intersection)[0])
        if pos > 0 and default_forms[pos - 1].isdigit():
            tokens.add("top")
            args["top"] = int(default_forms[pos - 1])
        if pos < len(default_forms) - 1 and default_forms[pos + 1] == "комикс":
            tokens.add("all")

    return tokens, args


def subtree_table(tree_view, table):
    res = rs.Table()
    names = [table[i][1] for i in range(len(table))]
    for node in tree_view.children():
        if node.node in names:
            res.append(table[names.index(node.node)])
    return res


def respond(tokens, args):
    if len(tokens) == 0:
        print(responses.bad_input())
        return True

    data = rs.Table()
    table = rs.Table()

    tree_view = from_json.load_tree("recommendation_system/dataset/tree.json")
    data.load("recommendation_system/dataset/table.txt")
    table.headers = data.headers[:]

    filters = rs.Filters()

    if "thanks" in tokens:
        response = responses.welcome()
        if "stop" not in tokens:
            response += ' ' + responses.reask()
        print(response)

    if "random" in tokens:
        choice = random.randint(0, len(data) - 1)
        print(responses.random_comics(), data[choice], sep='\n')
        print("\n" + responses.reask())
        return True

    if "name" in tokens:
        table.extend(data[:])
        table.sort(key=lambda el: damerau(el[1], args["name"]))
        dist = damerau(table[0][1], args["name"])
        print(responses.by_name(dist))
        if dist:
            num = 3
        else:
            num = 1

        tmp = table[:num]
        table.clear()
        table.extend(tmp)
        table.print()
        print("\n" + responses.reask())
        return True

    if "commercial" in tokens:
        subtree = None
        for el in tree_view.children():
            if el.node == "Коммерческие":
                subtree = el
        table.extend(subtree_table(subtree, data))
    else:
        if "novel" in tokens:
            subtree = None
            for el in tree_view.children():
                if el.node == "Графические романы":
                    subtree = el
            table.extend(subtree_table(subtree, data))

        if "asian" in tokens:
            subtree = None
            for el in tree_view.children():
                if el.node == "Восточные":
                    subtree = el
            table.extend(subtree_table(subtree, data))
        else:
            if "manga" in tokens:
                subtree = None
                for el in tree_view.children():
                    if el.node == "Манга":
                        subtree = el
                table.extend(subtree_table(subtree, data))

            if "manhwa" in tokens:
                subtree = None
                for el in tree_view.children():
                    if el.node == "Манхва":
                        subtree = el
                table.extend(subtree_table(subtree, data))

            if "manhua" in tokens:
                subtree = None
                for el in tree_view.children():
                    if el.node == "Маньхуа":
                        subtree = el
                table.extend(subtree_table(subtree, data))

        if "traditional" in tokens:
            subtree = None
            for el in tree_view.children():
                if el.node == "Традиционные":
                    subtree = el
            table.extend(subtree_table(subtree, data))
        else:
            if "marvel" in tokens:
                subtree = None
                for el in tree_view.children():
                    if el.node == "Marvel":
                        subtree = el
                table.extend(subtree_table(subtree, data))

            if "dc" in tokens:
                subtree = None
                for el in tree_view.children():
                    if el.node == "DC":
                        subtree = el
                table.extend(subtree_table(subtree, data))

    if "amateur" in tokens:
        subtree = None
        for el in tree_view.children():
            if el.node == "Любительские":
                subtree = el
        table.extend(subtree_table(subtree, data))
    else:
        if "web" in tokens:
            subtree = None
            for el in tree_view.children():
                if el.node == "Веб-комиксы":
                    subtree = el
            table.extend(subtree_table(subtree, data))

        if "strip" in tokens:
            subtree = None
            for el in tree_view.children():
                if el.node == "Стрипы":
                    subtree = el
            table.extend(subtree_table(subtree, data))

    if len(table) == 0 and tokens.intersection({"all", "top", "recommend"}):
        table.extend(data[:])

    if "tag" in tokens:
        for tag in args["tag"]:
            filters.containing_tags.add(tag)

    if "status" in tokens:
        filters.status = args["status"]

    if "rating" in tokens:
        filters.rating = args["rating"]

    if "minyear" in tokens or "maxyear" in tokens:
        filters.years_filter = True
        filters.years_range = [int(min(data, key=lambda a: int(a[3]))[3]), int(max(data, key=lambda a: int(a[3]))[3])]
        if "minyear" in tokens:
            filters.years_range[0] = args["minyear"]
        if "maxyear" in tokens:
            filters.years_range[1] = args["maxyear"]

    if "stop" in tokens:
        print(responses.goodbye())
        return False

    if "recommend" in tokens:
        print(responses.recommend())
        data.print()
        likes = list(map(int, input("\nВведите список номеров понравившихся комиксов:\n").replace(" ", "").split(',')))
        dislikes = list(
            map(int, input("\nВведите список номеров непонравившихся комиксов:\n").replace(" ", "").split(',')))
    else:
        likes = list()
        dislikes = list()

    if len(table):
        extended, res = rs.search(table, likes, dislikes, rs.euclidean_metric, filters)

        if "recommend" not in tokens:
            res.sort(key=lambda el: -int(el[5]))

        if "top" in tokens:
            tmp = res[:args["top"]]
            res.clear()
            res.extend(tmp)

        if extended:
            print("\n" + responses.extra_result())

        res.headers.pop(1)
        for el in res:
            el.pop(1)
        res.reformat()
        res.print()
        print("\n" + responses.reask())
    return True


if __name__ == "__main__":
    print(responses.hello() + ' ' + responses.ask_to_ask())

    go = True
    context = "greet"
    while go:
        phrase = input()
        tokens, args = tokenize(phrase, context)
        go = respond(tokens, args)
        context = "reask"
