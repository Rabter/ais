import random


def random_comics():
    responses = ["Случайный комикс:", "Например, этот:", "Сегодня удача улыбнулась ему:", "Тыкаем пальцем в небо...",
                 "Случайный так случайный:"]
    return responses[random.randint(0, len(responses) - 1)]


def by_name(dif):
    if dif == 0:
        responses = ["Знаю такой!", "Есть такой:", "Вот же он:"]
    else:
        responses = ["Похожие комиксы:", "Найдены вот такие:", "Напомнило эти:", "Самые похожие:",
                     "Первый раз слышу. Может, эти:"]
    return responses[random.randint(0, len(responses) - 1)]


def bad_input():
    responses = ["К сожалению, я Вас не понимаю.", "Я не знаю, как на это отвечать.", "Ваш запрос мне не понятен.",
                 "Что это значит?"]
    return responses[random.randint(0, len(responses) - 1)]


def extra_result():
    responses = ["Не найдено результатов по данному запросу. Возможно, Вам подойдут следующие предложения:",
                 "Очень сложный запрос, я знаю только такие:"]
    return responses[random.randint(0, len(responses) - 1)]


def recommend():
    responses = ["Конечно подскажу! Но сперва мне нужно знать, что Вам нравится.",
                 "Не вопрос. Пожалуйста, уточните свои предпочтения."]
    return responses[random.randint(0, len(responses) - 1)]


def goodbye():
    responses = ["До свидания!", "С удовольствием помогу снова!"]
    return responses[random.randint(0, len(responses) - 1)]


def hello():
    responses = ["Здравствуйте!"]
    return responses[random.randint(0, len(responses) - 1)]


def ask_to_ask():
    responses = ["С чем Вам помочь?", "Пожалуйста, введите запрос:"]
    return responses[random.randint(0, len(responses) - 1)]


def reask():
    responses = ["Помочь еще с чем-нибудь?", "Все еще нужна помощь?"]
    return responses[random.randint(0, len(responses) - 1)]


def welcome():
    np = ["На здоровье!", "Не вопрос.", "Без проблем.", "Обращайтесь.", "Это было не сложно."]
    return np[random.randint(0, len(np) - 1)]
