# Привет, второй курс.
# Переписал свой же код из https://gitlab.com/Rabter/algorithm-analysis/-/blob/master/lab_1/algorithms.cpp
# Сохраняйте свои лабы

def damerau(s1, s2):
    n = len(s1) + 1
    m = len(s2) + 1
    matrix = [[0 for _ in range(m)] for _ in range(n)]
    maxlen = max(n, m)
    for i in range(maxlen):
        if i < n:
            matrix[i][0] = i
        if i < m:
            matrix[0][i] = i

    for i in range(1, n):
        for j in range(1, m):
            idec = i - 1
            jdec = j - 1
            d1 = matrix[idec][jdec] + (s1[idec] != s2[jdec])
            d2 = matrix[idec][j] + 1
            d3 = matrix[i][jdec] + 1
            matrix[i][j] = min(d1, min(d2, d3))
            if idec and jdec and s1[idec] == s2[j - 2] and s1[i - 2] == s2[jdec]:
                matrix[i][j] = min(matrix[i][j], matrix[i - 2][j - 2] + 1)

    return matrix[n - 1][m - 1]

